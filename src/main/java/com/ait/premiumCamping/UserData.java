package com.ait.premiumCamping;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



@ManagedBean(name="UserDataBean")
@SessionScoped
public class UserData {

	public String firstName;
	public String surname;
	public String emailAddress;
	public String password;
	public static ArrayList<Users> myArrayListOfUsers;
	
	
	public static ArrayList<Users> getMyArrayListOfUsers() {
		return myArrayListOfUsers;
	}
	
	public void setMyArrayListOfUsers(ArrayList<Users> myArrayListOfUsers) {
		UserData.myArrayListOfUsers = myArrayListOfUsers;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}





	@PostConstruct
	public void init() {
		myArrayListOfUsers = new ArrayList<Users>();
		Users firstUser = new Users("William", "murphy", "william@gmail.com", "123");
		myArrayListOfUsers.add(firstUser);
		Users secondUser = new Users("Harry", "harry30", "harry@gmail.com", "456");
		myArrayListOfUsers.add(secondUser);
		Users thirdUser = new Users("Jon", "jon2021",  "jon@gmail.com", "789");
		myArrayListOfUsers.add(thirdUser);
	}
	
	
	// can delete this after, only using it to see if the arraylist is working 
	public void printOutArrayListOfRegisteredUsers() {
		for (int i = 0; i < myArrayListOfUsers.size(); i++) {
			System.out.println("all the registered users in the arraylist: " + myArrayListOfUsers.get(i));			
		}
	}

	public void saveNewUser() {
		Users myNewRegisteredUser = new Users(firstName, surname, emailAddress, password);
		
		System.out.println("the new user: " + myNewRegisteredUser);
		
		myArrayListOfUsers.add(myNewRegisteredUser);
		
		//to do: can delete this for loop. was only checking that the ArrayList was working correctly.
		for (int i = 0; i < myArrayListOfUsers.size(); i++) {
			System.out.println("all the registered users in the arraylist: " + myArrayListOfUsers.get(i));			
		}
		
	}
	
	// need to change this to email and password 
	public boolean validateUser(String userName, String password) {
		boolean validUser = false;
		for (Users user:myArrayListOfUsers) {
			if((user.getFirstName().equals(userName))&&(user.getFirstName().equals(userName) )){
				validUser=true;
				break;
			}
		}
		return validUser;
	}

}
