package com.ait.premiumCamping;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



@ManagedBean(name="UsersBean")
@SessionScoped
public class Users {
	
	//instance variables 
	private String firstName;
	private String surname;
	private String emailAddress;
	private String password;
	

	
	// default constructor
	public Users() {
		
	}
	
	//overloaded constructor
	public Users(String firstName, String surname, String emailAddress, String password) {
		this.firstName = firstName;
		this.surname = surname;
		this.emailAddress = emailAddress;
		this.password = password;
	}
	
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "The firstname is: " + getFirstName() + 
				". The surname is: " + getSurname() + 
				". The email is: " + getEmailAddress() +
				". The password is: " + getPassword() ;
	} 
	
	public void addNewRegistedUserToArrayList() {
		Users myNewRegisteredUser = new Users(firstName, surname, emailAddress, password);
		UserData.myArrayListOfUsers.add(myNewRegisteredUser);
		
		System.out.println("the new user: " + myNewRegisteredUser);		
	}
	
	

}
